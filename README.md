# Usage:

1. Open the editor and choose a map.
2. Save your empty map as `clever_name` in the `MPMissions` category.
3. Download the latest template files from GitLab.
4. Go to `Documents/Arma 3/MPMissions/clever_name.map`
5. Overwrite folder contents with downloaded template files.
6. In the editor, re-open your map to update with the new files.
7. Move the start block from the bottom left corner of the map to wherever you want to start.
   Right click -> transform -> snap to surface is quite useful when moving the start block.
8. Configure the Loadout and Respawn modules in the start block.
9. Delete unused groups such as Anvil or Ripper if they wont be used.

# Loadout Selection:

You have the following options:

Faction selects the uniforms/vest/packs and general _theme_ of the players for the mission. Equipment selects the weapons the players will be given. Having these as separate options allows you to mix-and-match as needed to get the right feeling for your mission.

 - Optics - enables the 1x optics the weapons are configured for (2x for the marksman)
 - Suppressors - enables the use of suppressors (not all weapons have them)
 - Rails - enables the rail attachments, i.e. lasers (not all weapons have them)
 - E-Tools - adds an E-Tool to each soldiers inventory
 - HALO - gives each player an altimeter watch, moves their pack to their chest, and gives them a parachute
 - NVGs - adds NVGs to each player, the type is defined by the faction selected.

# Respawn Settings:

This module allows you to enable/disable the respawn button in the spectate view.
The delay is how long a player must wait before the button shows up.
