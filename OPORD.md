
Instructions
============

Use this document to help write a simple text OPeration ORDer to describe your mission on the event post.
Just delete the instructions/template and edit it to fit your mission.


OPORD Template
==============

OP name
Date

SITUATION
Description of the context of the upcoming mission. I.e. tensions are high between A and B.

MISSION
3-4 sentence description of the mission the players will be executing

ENEMY FORCES
composition of enemy forces. Heavy armour/air really should be mentioned if it might show up

FRIENDLY FORCES
composition of friendly forces plus any major equipment they get like aircraft/tanks.

NOTES
any extra notes such as one-lifer etc.


Example
=======

Winter Blues
May 8, 1993

SITUATION
It has been 11 days since the USSR invaded Germany, Norway, and Takistan. They have been making significant progress on all fronts. The resistance in the last remaining holdout in the Caucasus region has also started to crumble with the occupation of the Chernarus region.

MISSION
EUCOM has identified the Chernarus region as a critical beachhead for operations to strike back into the heart of the USSR. Your task is to push north from a naval landing and re-establish abandoned OP Eagle overlooking one of the passes into the Chernarus region. This movement will be under the cover of weather to allow you to slip through the enemy lines, however, you can expect a counter-attack once the enemy understands your intention.

ENEMY FORCES
Motorized infantry with light armour support
No air threat due to acclimate weather conditions

FRIENDLY FORCES
1x Army Ranger Platoon
2x M2 HMMWV
1x Mk19 HMMWV
2x M2 tripod
2x M220 TOW launchers

NOTES
One life operation
